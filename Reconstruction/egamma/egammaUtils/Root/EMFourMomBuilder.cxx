/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */

#include "egammaUtils/EMFourMomBuilder.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "EventPrimitives/EventPrimitives.h"

namespace {
constexpr float el_mass = 0.510998;
constexpr float ph_mass = 0.0;

AmgMatrix(5, 5)
jacobian(const double phi, const int charge, const double momentum) {
  AmgMatrix(5, 5) jac;
  jac.setIdentity();
  jac(3, 3) = -1.* (1. / std::sin(phi));            // deta/dtheta
  jac(4, 4) = -1 * charge * (momentum * momentum);  // dp/d(q/p)
  return jac;
}

void
setFromCluster(xAOD::Egamma& eg)
{

  const xAOD::CaloCluster* cluster = eg.caloCluster();
  const float eta = cluster->eta();
  const float phi = cluster->phi();
  const float E = cluster->e();
  if (eg.type() == xAOD::Type::Electron) {
    const double pt =
      E > el_mass ? sqrt(E * E - el_mass * el_mass) / cosh(eta) : 0;
    eg.setP4(pt, eta, phi, el_mass);
  } else {
    eg.setP4(E / cosh(eta), eta, phi, ph_mass);
  }
}

void
setFromTrkCluster(xAOD::Electron& el)
{

  const xAOD::CaloCluster* cluster = el.caloCluster();
  const xAOD::TrackParticle* trackParticle = el.trackParticle();

  bool goodTrack = (xAOD::EgammaHelpers::numberOfSiHits(trackParticle) >= 4);
  const float E = cluster->e();
  const float eta = goodTrack ? trackParticle->eta() : cluster->eta();
  const float phi = goodTrack ? trackParticle->phi() : cluster->phi();

  const double pt =
    E > el_mass ? sqrt(E * E - el_mass * el_mass) / cosh(eta) : 0;
  el.setP4(pt, eta, phi, el_mass);

  // Electron with tracks all should  have a covariance matrix set
  AmgMatrix(4, 4) matrix;
  matrix.setZero();
  static const eg_resolution eg_resol("run2_pre");
  const float sigmaE_over_E = eg_resol.getResolution(el);
  matrix(0, 0) =
    (sigmaE_over_E * E * sigmaE_over_E * E) / (cosh(eta) * cosh(eta));

  // Direction can be filled only when a good track is there
  if (goodTrack) {
    xAOD::ParametersCovMatrix_t covmat =
        trackParticle->definingParametersCovMatrix();
    // Start from  the d0, z0, phi, theta, q/p
    // and make the (Et, eta, phi, M)
    AmgMatrix(5, 5) J = jacobian(trackParticle->phi(), trackParticle->charge(),
                                 trackParticle->p4().P());
    AmgMatrix(5, 5) m;
    m.setZero();
    m = J * (covmat * J.transpose());
    matrix(1, 1) = m(3, 3);
    matrix(2, 2) = m(2, 2);
    matrix.fillSymmetric(0, 1, m(4, 3));
    matrix.fillSymmetric(0, 2, m(4, 2));
    matrix.fillSymmetric(1, 2, m(3, 2));
  }

  el.setCovMatrix(matrix.cast<float>());
}

void
setFromTrkCluster(xAOD::Photon& ph)
{
  const xAOD::CaloCluster* cluster = ph.caloCluster();
  float E = cluster->e();
  float eta = cluster->eta();
  float phi = cluster->phi();
  Amg::Vector3D momentumAtVertex = xAOD::EgammaHelpers::momentumAtVertex(&ph);
  if (momentumAtVertex.mag() > 1e-5) { // protection against p = 0
    eta = momentumAtVertex.eta();
    phi = momentumAtVertex.phi();
  }
  ph.setP4(E / cosh(eta), eta, phi, ph_mass);
}
}

/////////////////////////////////////////////////////////////////

namespace EMFourMomBuilder
{
void
calculate(xAOD::Electron& electron) {
  if (electron.trackParticle()) {
    return setFromTrkCluster(electron);
  } else {
    setFromCluster(electron);
  }
}

void 
calculate(xAOD::Photon& photon) {
  if (xAOD::EgammaHelpers::conversionType(&photon) ==
      xAOD::EgammaParameters::doubleSi) {
    setFromTrkCluster(photon);
  } else {
    setFromCluster(photon);
  }
}
}

