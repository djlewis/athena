/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file Accessor.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Oct, 2023
 * @brief Helper class to provide type-safe access to aux data.
 */


#include "AthContainers/AuxTypeRegistry.h"
#include "AthContainers/exceptions.h"


namespace SG {


/**
 * @brief Constructor.
 * @param name Name of this aux variable.
 *
 * The name -> auxid lookup is done here.
 */
template <class T, class ALLOC>
inline
Accessor<T, ALLOC>::Accessor (const std::string& name)
  : ConstAccessor<T, ALLOC> (name)
{
}


/**
 * @brief Constructor.
 * @param name Name of this aux variable.
 * @param clsname The name of its associated class.  May be blank.
 *
 * The name -> auxid lookup is done here.
 */
template <class T, class ALLOC>
inline
Accessor<T, ALLOC>::Accessor (const std::string& name,
                              const std::string& clsname)
  : ConstAccessor<T, ALLOC> (name, clsname)
{
}


/**
 * @brief Constructor taking an auxid directly.
 * @param auxid ID for this auxiliary variable.
 *
 * Will throw @c SG::ExcAuxTypeMismatch if the types don't match.
 */
template <class T, class ALLOC>
inline
Accessor<T, ALLOC>::Accessor (const SG::auxid_t auxid)
  : ConstAccessor<T, ALLOC> (auxid)
{
}


/**
 * @brief Fetch the variable for one element, as a non-const reference.
 * @param e The element for which to fetch the variable.
 */
template <class T, class ALLOC>
template <class ELT>
ATH_REQUIRES( IsAuxElement<ELT> )
inline
typename Accessor<T, ALLOC>::reference_type
Accessor<T, ALLOC>::operator() (ELT& e) const
{
  assert (e.container() != 0);
  return e.container()->template getData<T> (this->m_auxid, e.index());
}


/**
 * @brief Fetch the variable for one element, as a non-const reference.
 * @param container The container from which to fetch the variable.
 * @param index The index of the desired element.
 *
 * This allows retrieving aux data by container / index.
 * Looping over the index via this method will be faster then
 * looping over the elements of the container.
 */
template <class T, class ALLOC>
inline
typename Accessor<T, ALLOC>::reference_type
Accessor<T, ALLOC>::operator() (AuxVectorData& container,
                                size_t index) const
{
  return container.template getData<T> (this->m_auxid, index);
}


/**
 * @brief Set the variable for one element.
 * @param e The element for which to fetch the variable.
 * @param x The variable value to set.
 */
template <class T, class ALLOC>
template <class ELT>
ATH_REQUIRES( IsAuxElement<ELT> )
inline
void Accessor<T, ALLOC>::set (ELT& e, const element_type& x) const
{
  (*this)(e) = x;
}


/**
 * @brief Get a pointer to the start of the auxiliary data array.
 * @param container The container from which to fetch the variable.
 */
template <class T, class ALLOC>
inline
typename Accessor<T, ALLOC>::container_pointer_type
Accessor<T, ALLOC>::getDataArray (AuxVectorData& container) const
{
  return reinterpret_cast<container_pointer_type>
    (container.getDataArray (this->m_auxid));
}


/**
 * @brief Get a span over the auxilary data array.
 * @param container The container from which to fetch the variable.
 */
template <class T, class ALLOC>
inline
typename Accessor<T, ALLOC>::span
Accessor<T, ALLOC>::getDataSpan (AuxVectorData& container) const
{
  auto beg = reinterpret_cast<container_pointer_type>
    (container.getDataArray (this->m_auxid));
  return span (beg, container.size_v());
}


/**
 * @brief Test to see if this variable exists in the store and is writable.
 * @param e An element of the container which to test the variable.
 */
template <class T, class ALLOC>
template <class ELT>
ATH_REQUIRES( IsAuxElement<ELT> )
inline
bool
Accessor<T, ALLOC>::isAvailableWritable (ELT& e) const
{
  return e.container() && e.container()->isAvailableWritable (this->m_auxid);
}


} // namespace SG
